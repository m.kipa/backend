<?php
const NOT_CHECK_PERMISSIONS = true;
const NO_AGENT_CHECK = true;
$_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__);
include  $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

return [
    'paths'         => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
    ],
    'templates'     => [
        'file' => '%%PHINX_CONFIG_DIR%%/db/templates/basic.txt',
    ],
    'environments'  => [
        'default_migration_table' => 'phinxlog',
        'default_environment'     => 'production',
        'production'              => [
            'adapter'              => 'mysql',
            'host'                 => 'db',
            'name'                 => 'db',
            'user'                 => 'db',
            'pass'                 => 'db',
            'port'                 => '3306',
            'charset'              => 'utf8',
            'collation'            => 'utf8_unicode_ci',
        ],
    ],
    'version_order' => 'creation',
];
