<?php

namespace Itfund\Main\Controllers;

use Bitrix\Main\Engine\ActionFilter\Authentication;
use Bitrix\Main\Engine\ActionFilter\Csrf;
use \Bitrix\Main\Engine\Controller;
use Bitrix\Main\Loader;
use Itfund\Main\Services\NewsServices;

class News extends Controller
{
    public function init(): void
    {
        parent::init();
        Loader::includeModule('iblock');
    }

    public function configureActions(): array
    {
        return [
            'list' => [
                '-prefilters' => [
                    Csrf::class,
                    Authentication::class
                ],
            ],
            'get' => [
                '-prefilters' => [
                    Csrf::class,
                    Authentication::class
                ],
            ],
        ];
    }

    public function listAction(): ?array
    {
        return (new NewsServices())->getNewsList();
    }

    public function getAction($id): ?array
    {
        return (new NewsServices())->getNewsItems($id);
    }
}