<?php

namespace Itfund\Main\Controllers;

use Bitrix\Main\Engine\ActionFilter\Authentication;
use Bitrix\Main\Engine\ActionFilter\Csrf;
use Bitrix\Main\Engine\Controller;
use Bitrix\Main\Loader;
use Itfund\Main\Services\LinuxSoftServices;

class LinuxSoft extends Controller
{
    public function init()
    {
        parent::init();
        Loader::includeModule('iblock');
    }
    
    public function configureActions(): array
    {
        return [
            'list' => [
                '-prefilters' => [
                    Csrf::class,
                    Authentication::class
                ],
            ],
            'get' => [
                '-prefilters' => [
                    Csrf::class,
                    Authentication::class
                ],
            ],
        ];
    }
    
    public function listAction(): ?array
    {
        return (new LinuxSoftServices())->getLinuxSoftList();
    }
    
    public function getAction($code): ?array
    {
        return (new LinuxSoftServices())->getlinuxSoftItems($code);
    }
}
