<?php

namespace Itfund\Main\Services;

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Entity\Query\Join;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\UserTable;
use http\Exception\RuntimeException;

class LinuxSoftServices
{
    public function getLinuxSoftList(): ?array
    {
        
        $entity = IblockTable::compileEntity('linuxsoft');
        if(!$entity) {
            throw new RuntimeException('неправильный api code');
        }
        
        $object = $entity->getIblock()->getEntityDataClass();
        $query = $object::query()
            ->addSelect('NAME')
            ->addSelect('PREVIEW_TEXT')
            //->addSelect('PREVIEW_PICTURE')
            ->where('ACTIVE', 'Y')
            ->fetchCollection();
        
        //@TODO можно сразу в лист вывести кем и когда создана, иногда на порталах, форумах и новостных сайтах так делают
        $items = [];
        foreach ($query as $item) {
            $items = [
                'name' => $item->getName(),
                'text' => $item->getPreviewText(),
                //'previewPicture' => \CFile::GetFileArray($item->getPrviewPicture())['SRC']
            ];
        }
        return $items;
    }
    
    public function getLinuxSoftItems($code): ?array
    {
        
        $entity = IblockTable::compileEntity('linuxsoft');
        if (!$entity) {
            throw new RuntimeException('неправильный api code');
        }
        
        $object = $entity->getIblock()->getEntityDataClass();
        $query = $object::query()
            ->addSelect('NAME')
            ->addSelect('CODE')
            ->addSelect('PREVIEW_PICTURE')
            ->addSelect('PREVIEW_TEXT')
            ->addSelect('DETAIL_TEXT')
            // ->registerRuntimeField('CREATED_BY', new Reference(
            //     'CREATED_BY',
            //     UserTable::class,
            //     Join::on('this.CREATED_BY.VALUE', 'ref.ID')
            // ))
            // ->addSelect('CREATED_BY.NAME')
            // ->registerRuntimeField(new Reference(
            //     'MODIFIED_BY',
            //     UserTable::class,
            //     Join::on('this.MODIFIED_BY', 'ref.ID')
            // ))
            ->where('CODE', $code)
            ->fetchCollection();
        //->getQuery();
        
        $items = [];
        foreach ($query as $item) {
            $items[] = [
                'name' => $item->getName(),
                'code' => $item->getCode(),
                'picture' => \CFile::GetFileArray($item->getPreviewPicture())['SRC'],
                'previewText' => $item->getPreviewText(),
                'detailText' => $item->getDetailText(),
                //'createdBy' => $item->getCreatedBy()->getValue(),
                //'modifiedBy' => $item->getModifiedBy()->getValue(),
            ];
        }
        return $items;
    }
}
