<?php

namespace Itfund\Main\Services;

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Context;
use http\Exception\RuntimeException;

class NewsServices
{
    public function getNewsItems($id): ?array
    {
        $apiCode = $this->getApicode();

        $entity = IblockTable::compileEntity($apiCode);
        if (!$entity) {
            throw new RuntimeException('Wrong API code');
        }

        $object = $entity->getIblock()->getEntityDataClass();
        $query = $object::query()
            ->addSelect('ID')
            ->addSelect('NAME')
            ->where('ID', $id);

        $collection = $query->fetchCollection();

        $items = [];
        foreach ($collection as $item) {
            $items[] = [
                'id' => $item->getId(),
                'name' => $item->getName()
            ];
        }
        return $items;
    }

    public function getNewsList(): ?array
    {
        $apiCode = $this->getApiCode();

        $entity = IblockTable::compileEntity($apiCode);
        if (!$entity) {
            throw new \RuntimeException('Wrong API code');
        }

        $object = $entity->getIblock()->getEntityDataClass();
        $query = $object::query()
            ->addSelect('NAME');

        $collection = $query->fetchCollection();

        $items = [];
        foreach ($collection as $item) {
            $items[] = [
                'name' => $item->getName(),
            ];
        }
        return $items;
    }

    private function getApiCode(): string
    {
        $parts = explode('.', Context::getCurrent()->getRequest()->getHttpHost());

        return 'News' . ucfirst(strtolower(mb_strlen($parts[0]) === 2 ? $parts[0] : ''));
    }
}
