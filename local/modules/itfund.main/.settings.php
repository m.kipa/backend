<?php

return [
    'controllers' => [
        'value' => [
            'defaultNamespace' => '\\Itfund\\Main\\Controllers',
        ],
        'readonly' => true,
    ]
];