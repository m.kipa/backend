<?php


use Bitrix\Main\ModuleManager;

class itfund_main extends CModule
{
    public function __construct()
    {
        $arModuleVersion = [];

        include __DIR__ . '/version.php';

        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_ID = 'itfund.main';
        $this->MODULE_NAME = 'Основной модуль';
        $this->MODULE_DESCRIPTION = 'Бизнес логика';
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = 'webpractik';
        $this->PARTNER_URI = 'https://webpractik.ru';
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
    }

    public function doUninstall()
    {
        ModuleManager::unregisterModule($this->MODULE_ID);
    }

}