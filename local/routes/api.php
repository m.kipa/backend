<?php

use Bitrix\Main\Routing\RoutingConfigurator;
use \Itfund\Main\Controllers\News;
use \Itfund\Main\Controllers\LinuxSoft;

return static function (RoutingConfigurator $routes) {
    $routes->prefix('api')->group(function (RoutingConfigurator $routes) {
        $routes->get('documentation', [\BitrixOA\BitrixUiController::class, 'apidocAction']);

        //News
        $routes->prefix('news')->group(function (RoutingConfigurator $routes){
            $routes->get('list', [News::class, 'list']);
            $routes->get('get/{id}', [News::class, 'get']);
        });

        //Projects
        $routes->prefix('projects')->group(function (RoutingConfigurator $routes){
            $routes->get('list', [Projects::class, 'list']);
            $routes->get('get/{id}', [Projects::class, 'get']);
        });
        
        //Homework
        $routes->prefix('linuxsoft')->group(function (RoutingConfigurator $routes){
            $routes->get('list', [LinuxSoft::class, 'list']);
            $routes->get('get/{code}', [LinuxSoft::class, 'get']);
        });
    });
};
