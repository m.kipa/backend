<?php

use Bitrix\Main\Loader;
use Phinx\Migration\AbstractMigration;

class CreateIblockType extends AbstractMigration
{
    private $iblockType = [
        'code' => 'content',
        'name' => 'Контент'
    ];
    
    /**
     * @throws \Bitrix\Main\LoaderException
     */
    public function init(): void
    {
        Loader::includeModule('iblock');
    }
    
    public function up(): void
    {
        
        $obIBlockType = new CIBlockType;
        $arFields = [
            'ID' => $this->iblockType['code'],
            'SECTIONS' => 'Y',
            'LANG' => [
                'ru' => [
                    'NAME' => $this->iblockType['name']
                ]
            ]
        ];
        
        $addedIblock = $obIBlockType->Add($arFields);
        if (!$addedIblock) {
            throw new RuntimeException('Failed to create iblock type. Error: ' . $obIBlockType->LAST_ERROR);
        }
    }
    
    public function down(): void
    {
        $iblockType = \Bitrix\Iblock\TypeTable::query()
            ->whereIn('ID', array_column($this->iblockType, 'code'))
            ->fetchCollection();
        
        if ($iblockType === null) {
            return;
        }
        
        CIBlockType::Delete($iblockType->getId());
    }
}
