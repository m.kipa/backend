<?php

use Bitrix\Main\Loader;
use Phinx\Migration\AbstractMigration;

class $className extends AbstractMigration
{
    /**
     * @throws \Bitrix\Main\LoaderException
     */
    public function init(): void
    {
        Loader::includeModule('iblock');
    }

    public function up(): void
    {
        //
    }

    public function down(): void
    {
        //
    }
}
